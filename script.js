let changeGama = document.querySelector(".changeGama");
let theme = localStorage.getItem("theme");
let root = document.documentElement;
changeGama.addEventListener("click", function () {
  getComputedStyle(root).getPropertyValue("--white-color") === "white"
    ? dark()
    : light();
});
function dark() {
  root.style.setProperty("--white-color", "black");
  root.style.setProperty("--black-color", "white");
  root.style.setProperty("--photo-filtr", "invert(1)");
  root.style.setProperty("--menu-background", "#164464");
  root.style.setProperty("--header-color", "#f74848");
  localStorage.setItem("theme", "dark");
}
function light() {
  root.style.setProperty("--white-color", "white");
  root.style.setProperty("--black-color", "black");
  root.style.setProperty("--photo-filtr", "invert(0)");
  root.style.setProperty("--menu-background", "#35444f");
  root.style.setProperty("--header-color", "#4bcaff");
  localStorage.setItem("theme", "light");
}

document.addEventListener(
  "DOMContentLoaded",
  theme === "dark" ? dark() : light()
);
